<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function() {
    return view('about');
})->name('about');

Route::get('/about_manage', function() {
    return view('about_manage');
})->name('about_manage');

Route::get('/about_insurance', function() {
    return view('about_insurance');
})->name('about_insurance');

Route::get('/about_bond', function() {
    return view('about_bond');
})->name('about_bond');

Route::get('/about_safety', function() {
    return view('about_safety');
})->name('about_safety');

Route::get('/services_demo', function() {
    return view('services_demo');
})->name('services_demo');

Route::get('/services_carting', function() {
    return view('services_carting');
})->name('services_carting');

Route::get('/services_cleaning', function() {
    return view('services_cleaning');
})->name('services_cleaning');

Route::get('/experiences', function() {
    return view('experiences');
})->name('experiences');

Route::get('/experiences_project', function() {

    $projects = [

        [
            'company_name' => 'JRM Construction',
            'address_line_1' => '242 West 36th Street, 11th Floor',
            'address_line_2' => 'New York, NY 10018',
            'project_line_1' => '50 Rockefeller Center',
            'year' => '2021 - Present',
            'price' => '$1,600,000.00',
            'description' => 'Interior Demolition',
            'contact' => 'Christina Estrada - (646) 937 1879',
            'image' => '9.jpg',
            'sort_order' => 1
        ],

        [
            'company_name' => 'Unity Construction',
            'address_line_1' => '137 Varick Street, Suite 404',
            'address_line_2' => 'New York, NY 10013',
            'project_line_1' => 'Ewalk - 247 West 42nd Street',
            'year' => '2019 - 2020',
            'price' => '$877,000.00',
            'description' => 'Structural Demolition, Architectural Demolition, MEP',
            'contact' => 'Charlie Drayton - (917) 513 5534',
            'image' => '16.jpg',
            'sort_order' => 2
        ],

        [
            'company_name' => 'Skanska',
            'address_line_1' => '350 5th Avenue, 32nd Floor',
            'address_line_2' => 'New York, NY 10118',
            'project_line_1' => 'Skylights - 1000 5th Avenue',
            'year' => '2018 - 2019',
            'price' => '$3,463,500.00',
            'description' => 'Interior Demolition, Structural Demolition, Skylights, MEP',
            'contact' => 'Andy Cristi - (973) 289 1859',
            'image' => '14.jpg',
            'sort_order' => 3
        ],

        [
            'company_name' => 'WDF Inc.',
            'address_line_1' => '30 N MacQuesten Pkwy',
            'address_line_2' => 'Mount Vernon, NY 10550',
            'project_line_1' => 'Grand Central Terminal',
            'year' => '2018 - 2019',
            'price' => '$326,800.00',
            'description' => 'Interior Demolition',
            'contact' => 'Mr. Adam Goldson - (914) 393 7167',
            'image' => '13.jpg',
            'sort_order' => 4
        ],

        [
            'company_name' => 'Humphrey Rich',
            'address_line_1' => '10200 Old Columbia Road',
            'address_line_2' => 'Columbia, Maryland 21046',
            'project_line_1' => 'Essex House',
            'year' => '2017 - 2018',
            'price' => '$1,200,000.00',
            'description' => 'Interior Demolition',
            'contact' => 'Mr. Tom Garcia - (204) 793 6556',
            'image' => '11.jpg',
            'sort_order' => 5
        ],

        [
            'company_name' => 'Hunter Roberts',
            'address_line_1' => '55 Water Street, 51st Floor',
            'address_line_2' => 'New York, NY 10041',
            'project_line_1' => '1 North End Avenue',
            'year' => '2014 - 2016',
            'price' => '$2,248,470.00',
            'description' => 'Interior Demolition / Mechanical Demolition, Structural Demolition',
            'contact' => 'Mr. Dan Halajian - (646) 341 3379',
            'image' => '15.jpg',
            'sort_order' => 6
        ],

        [
            'company_name' => 'Sciame',
            'address_line_1' => '14 Wall Street, 2nd Floor',
            'address_line_2' => 'New York, NY 10005',
            'project_line_1' => '404 Park Avenue South',
            'year' => '2012 - 2014',
            'price' => '$1,679,831.36',
            'description' => 'Interior Demolition',
            'contact' => 'Mr. Michael Pardee - (917) 416 2017',
            'image' => '10.jpg',
            'sort_order' => 7
        ],

        [
            'company_name' => 'RC Dolner LLC',
            'address_line_1' => '15-17 East 17th Street',
            'address_line_2' => 'New York, NY 10003',
            'project_line_1' => 'Lincoln Center Promenade & Harmony Atrium',
            'project_line_2' => 'New York, NY',
            'year' => '2008 - 2009',
            'price' => '$2,870,000.00',
            'description' => 'Removal of Interior Finishes',
            'contact' => 'Mr. Lad McGuffey - (212) 645 2190',
            'image' => '6.jpg',
            'sort_order' => 8
        ],

        [
            'company_name' => 'Lend Lease',
            'address_line_1' => '200 Park Avenue, 9th Floor',
            'address_line_2' => 'New York, NY 10166',
            'project_line_1' => 'American Museum of Natural History',
            'project_line_2' => 'New York, NY',
            'year' => '2008 - 2009',
            'price' => '$552,000.00',
            'description' => 'Interior demolition',
            'contact' => 'Mr. Richard Samuelsen - (212) 485 9297',
            'image' => '2.jpg',
            'sort_order' => 9
        ],

        [
            'company_name' => 'G. Builders IV',
            'address_line_1' => '50 Broad Street',
            'address_line_2' => 'New York, NY 10004',
            'project_line_1' => '15 East 26th Street, New York, NY',
            'year' => '2007 - 2009',
            'price' => '$1,507,000.00',
            'description' => '	Interior and Structural Demolition',
            'contact' => 'Mr. George Figliolia - (212) 635 0760',
            'image' => '7.jpg',
            'sort_order' => 10
        ],

        [
            'company_name' => 'FJ. Sciame Construction Co., Inc.',
            'address_line_1' => '14 Wall Street',
            'address_line_2' => 'New York, NY 10005',
            'project_line_1' => 'The Mark Hotel',
            'project_line_2' => '25 East 77th Street, New York, NY',
            'year' => '2008 - 2009',
            'price' => '$552,000.00',
            'description' => 'Interior demolition',
            'contact' => 'Mr. Richard Samuelsen - (212) 485 9297',
            'image' => '3.jpg',
            'sort_order' => 11
        ],










        [
            'company_name' => 'FJ. Sciame Construction Co., Inc.',
            'address_line_1' => '14 Wall Street',
            'address_line_2' => 'New York, NY 10005',
            'project_line_1' => '160 Fifth Avenue, New York, NY',
            'year' => '2007 - 2009',
            'price' => '$1,527,000.00',
            'description' => 'Removal of Interior Finishes and Structural Removals',
            'contact' => 'Mr. Anthony Frontino - (212) 232 2203',
            'image' => '4.jpg',
            'sort_order' => 12
        ],













    ];

    return view('experiences_project')->with(['projects' => $projects]);
})->name('experiences_project');

Route::get('/licenses', function() {
    return view('licenses');
})->name('licenses');

Route::get('/contact', function() {
    return view('contact');
})->name('contact');

Route::get('/links', function() {
    return view('links');
})->name('links');
