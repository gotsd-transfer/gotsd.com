<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>T S D - Tri State Dismantling</title>
    <link href="{{ asset('stylesheet.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    @livewireStyles
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        function reCaptchaCallback(response) {
            var el = document.getElementById('recaptcha');
            el.value = response;
            el.dispatchEvent(new Event('input'));
        }
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('analytics.key') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ config('analytics.key') }}');
    </script>
</head>

<body>
<div id="wrapper">
    <div id="header"><img src="{{ asset('images/TSD_header_031814.jpg') }}" width="840"/><br />
        <ul id="nav">
            <li id="liHome" class="off"><a href="{{ route('home') }}"><span>Home</span></a>
            </li>
            <li id="liAboutUs" class="off"><a href="{{ route('about') }}"><span>About Us</span></a>
                <ul >
                    <li><a href="{{ route('about_manage') }}">MANAGEMENT TEAM</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_insurance') }}">INSURANCE</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_bond') }}">BOND</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_safety') }}">HEALTH &amp; SAFETY</a></li>
                </ul></li>
            <li id="liServices" class="off"><a href="#"><span>Services</span></a>
                <ul>
                    <li><a href="{{ route('services_demo') }}">DEMOLITION/DISMANTLING</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('services_carting') }}">CARTING/RECYCLING</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('services_cleaning') }}">CONSTRUCTION CLEANING</a></li>
                </ul></li>
            <li id="liExperiences" class="off"><a href="{{ route('experiences') }}"><span>Experiences</span></a>
                <ul>
                    <li><a href=""></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li><a href="{{ route('experiences_project') }}">PROJECT REFERENCE</a></li>
                </ul></li>
            <li id="liLicenses" class="off"><a href="{{ route('licenses') }}"><span>Licenses &amp; Approvals</span></a>
            </li>
            <li id="liContact" class="off"><a href="{{ route('contact') }}"><span>Contact</span></a>
            </li>
            <li id="liLinks" class="off"><a href="{{ route('links') }}"><span>Related Links</span></a>
            </li>
        </ul>

        <img src="{{ asset('images/main_contact2.jpg') }}" width="840" height="200" /><br />
        <img src="{{ asset('images/bar_top.gif') }}" width="840" height="17" /> </div><div id="body">
        <div id="img"><img src="{{ asset('images/TSD_header_contact.jpg') }}" alt="" width="350" /></div>
        <div id="column1">
            <p class="text_home"></p>
            <p class="text_home"><strong><a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=207+Dupont+Street+Brooklyn,+NY+11222&amp;sll=40.783434,-73.96625&amp;sspn=0.184051,0.307617&amp;ie=UTF8&amp;hq=&amp;hnear=207+Dupont+St,+Brooklyn,+Kings,+New+York+11222&amp;ll=40.736527,-73.952494&amp;spn=0.023023,0.038452&amp;" target="_blank"><img src="{{ asset('images/map.jpg') }}" alt="Map" width="580" height="173" hspace="3" vspace="0" border="0" align="right" /></a>Tri-State Dismantling</strong><br />
                207 Dupont Street<br />
                Brooklyn, NY 11222<br />
                T:  (718) 349-2552
                <br />
                F:  (718) 349-0289<br />
                E-mail: mail[at]gotsd.com</p>
            <p class="text_home"><br />
            </p>
            <table width="100%" border="0" cellspacing="2" cellpadding="4">
                <tr class="p_bold">
                    <td valign="top" bgcolor="#CCCCCC"><strong>General Inquiry</strong></td>
                    <td valign="top" bgcolor="#CCCCCC"><strong>Administrative</strong></td>
                    <td valign="top" bgcolor="#CCCCCC"><strong>Sales/Estimating</strong></td>
                    <td valign="top" bgcolor="#CCCCCC"><strong>Carting/Job Scheduling</strong></td>
                    <td valign="top" bgcolor="#CCCCCC"><strong>Project Coordination</strong></td>
                </tr>
                <tr>
                    <!-- General Inquiry -->
                    <td width="20%" valign="top">
                        <p>
                            <em>mail[at]gotsd.com</em>
                        </p>

                        <p>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#contact-modal">
                                Contact Us
                            </button>


                            @livewire('contact-form')



                        </p>
                    </td>

                    <!-- Administrative -->
                    <td width="20%" valign="top">
                        <p>
                            Ewelina Szczepanik<br />
                            <em>Eve[at]gotsd.com</em>
                        </p>

                        <p>
                            Julia Kotowski<br />
                            <em>Julia[at]gotsd.com</em>
                        </p>

                    </td>

                    <!-- Sales/Estimating -->
                    <td width="20%" valign="top">
                        <p>
                            Richard Flamio<br />
                            <em>Richard[at]gotsd.com</em>
                        </p>
                        <p>
                            James Gildea<br />
                            <em>James[at]gotsd.com</em>
                        </p>
                        <p>
                            Chris Milos<br />
                            <em>CMilos[at]gotsd.com</em>
                        </p>
                        <p>
                            Matthew Szewczyk<br />
                            <em>Matthew[at]gotsd.com</em>
                        </p>

                        <p>
                            Oleh Dnistrian<br />
                            <em>Oleh[at]gotsd.com</em>
                        </p>
                    </td>

                    <!-- Carting/Job Scheduling -->
                    <td width="20%" valign="top">
                        <p>
                            Lukas Trzciniecki<br />
                            <em>Lukas[at]gotsd.com</em>
                        </p>
                    </td>

                    <!-- Project Coordination -->
                    <td width="20%" valign="top">

                        <p>
                            Thomas Llivirumbay<br />
                            <em>Thomas[at]gotsd.com</em>
                            </a>
                        </p>
                        <p>
                            Carlos Cando<br/>
                            <em>CCando[at]gotsd.com</em>
                        </p>

                    </td>

                </tr>
            </table>
            <p>Note: The email addresses above are formatted to prevent spam in our inboxes.</p>
        </div>
        <div id="body2"><img src="{{ asset('images/bar_bottom.gif') }}" width="840" height="15" /></div></div><div id="footer" class="footer">Copyright © 2003-2010 TSD, All Rights Reserved. | website created by <a href="http://www.bermangrp.com" target="_blank"><span style="color:#FFF">The Berman Group</span></a></div>
</div>
<script>
    var myModal = new bootstrap.Modal(document.getElementById('contact-modal'))
    window.addEventListener('closeModal', event=> {
        myModal.hide()
    })
</script>

@livewireScripts
<script>
    Livewire.on('resetReCaptcha', () => {
        grecaptcha.reset();
    });
</script>
</body>
</html>
