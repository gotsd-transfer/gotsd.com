<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>T S D - Tri State Dismantling</title>
    <link href="{{ asset('stylesheet.css') }}" rel="stylesheet" type="text/css" />
    <script language="JavaScript" src="{{ asset('ieHover.js') }}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('analytics.key') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ config('analytics.key') }}');
    </script>
</head>

<body>
<div id="wrapper">
    <div id="header"><img src="{{ asset('images/TSD_header_031814.jpg') }}" width="840"/><br />
        <ul id="nav">
            <li id="liHome" class="off"><a href="{{ route('home') }}"><span>Home</span></a>
            </li>
            <li id="liAboutUs" class="off"><a href="{{ route('about') }}"><span>About Us</span></a>
                <ul >
                    <li><a href="{{ route('about_manage') }}">MANAGEMENT TEAM</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_insurance') }}">INSURANCE</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_bond') }}">BOND</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_safety') }}">HEALTH &amp; SAFETY</a></li>
                </ul></li>
            <li id="liServices" class="off"><a href="#"><span>Services</span></a>
                <ul>
                    <li><a href="{{ route('services_demo') }}">DEMOLITION/DISMANTLING</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('services_carting') }}">CARTING/RECYCLING</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('services_cleaning') }}">CONSTRUCTION CLEANING</a></li>
                </ul></li>
            <li id="liExperiences" class="off"><a href="{{ route('experiences') }}"><span>Experiences</span></a>
                <ul>
                    <li><a href=""></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li><a href="{{ route('experiences_project') }}">PROJECT REFERENCE</a></li>
                </ul></li>
            <li id="liLicenses" class="off"><a href="{{ route('licenses') }}"><span>Licenses &amp; Approvals</span></a>
            </li>
            <li id="liContact" class="off"><a href="{{ route('contact') }}"><span>Contact</span></a>
            </li>
            <li id="liLinks" class="off"><a href="{{ route('links') }}"><span>Related Links</span></a>
            </li>
        </ul>
        <img src="{{ asset('images/main_about3.jpg') }}" alt="" width="840" height="200" /><br />
        <img src="{{ asset('images/bar_top.gif') }}" width="840" height="17" /> </div><div id="body">
        <div id="img"><img src="{{ asset('images/TSD_header_about_us.jpg') }}" alt="" width="350" /></div>
        <div id="column1">

            <h1>Tri-State Dismantling Corporation (TSD) is a full service demolition and dismantling contractor that specializes in providing superior service to all clients.
            </h1>
            <p>TSD’s reputation for consistently providing clients with quality, cost-effective demolition and dismantling services is backed by years of experience in the industry. Our seasoned professionals service the Tri-State area with superior attention to the needs and specifications of every job.          </p>
            <p>From project inception to complete, the experts at TSD can be counted on to adhere to the highest standards of professionalism while providing efficient solutions for the most challenging projects. </p>
        </div><div id="body2"><img src="{{ asset('images/bar_bottom.gif') }}" width="840" height="15" />
        </div></div><div id="footer" class="footer">Copyright © 2003-2010 TSD, All Rights Reserved. | website created by <a href="http://www.bermangrp.com" target="_blank"><span style="color:#FFF">The Berman Group</span></a></div>
</div>

</body>
</html>
