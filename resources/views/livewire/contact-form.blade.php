<div wire:ignore.self class="modal fade" id="contact-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send Message</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form id="contact-form" wire:submit.prevent="submit">
                    @csrf

                    <p>
                        Fields with an asterisk (<span class="text-danger">*</span>) are required.
                    </p>

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-2">
                                <label for="name">Name <span class="text-danger">*</span></label>
                                <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter your name" value="{{ old('name') }}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-2">
                                <label for="email">Email <span class="text-danger">*</span></label>
                                <input wire:model="email" type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Enter your email" value="{{ old('email') }}">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-2">
                        <label for="phone">Phone</label>
                        <input wire:model="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" placeholder="Enter your phone number" value="{{ old('phone') }}">
                        <div id="phoneHelp" class="form-text">Optional</div>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                        @enderror
                    </div>

                    <div class="form-group mb-2">
                        <label for="subject">Subject <span class="text-danger">*</span></label>
                        <input wire:model="subject" type="text" class="form-control @error('subject') is-invalid @enderror" id="subject" name="subject" placeholder="Enter the subject" value="{{ old('subject') }}">
                        @error('subject')
                        <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                        @enderror
                    </div>

                    <div class="form-group mb-2">
                        <label for="message">Message <span class="text-danger">*</span></label>
                        <textarea  wire:model="message" class="form-control @error('message') is-invalid @enderror" name="message" id="message" rows="5">{{ old('message') }}</textarea>
                        @error('message')
                        <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                        @enderror
                    </div>


                    <div class="form-group mb-2">
                        <input wire:model="recaptcha" type="text" class="form-control d-none @error('recaptcha') is-invalid @enderror" id="recaptcha" name="recaptcha" placeholder="Enter the recaptcha"">

                        <div wire:ignore>
                            <div class="g-recaptcha" data-callback="reCaptchaCallback" data-sitekey="6Lc9t4AbAAAAAOAMTR66vnIc9sYg93a_GJTVidZN"></div>
                        </div>

                        @error('recaptcha')
                        <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                        @enderror
                    </div>



                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" wire:click="submit">Send Message</button>
            </div>
        </div>
    </div>
</div>
