
<p> Subject: {{ $subject }} </p>
<p> Name: {{ $name }} </p>
<p> Email: {{ $email }} </p>
@isset($phone)
<p> Phone: {{ $phone }} </p>
@endisset
<p> Message: {{ $comment }} </p>