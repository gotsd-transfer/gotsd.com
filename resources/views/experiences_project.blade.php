<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>T S D - Tri State Dismantling</title>
    <link href="{{ asset('stylesheet.css') }}" rel="stylesheet" type="text/css" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('analytics.key') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ config('analytics.key') }}');
    </script>
</head>

<body>
<div id="wrapper">
    <div id="header"><img src="{{ asset('images/TSD_header_031814.jpg') }}" width="840"/><br />
        <ul id="nav">
            <li id="liHome" class="off"><a href="{{ route('home') }}"><span>Home</span></a>
            </li>
            <li id="liAboutUs" class="off"><a href="{{ route('about') }}"><span>About Us</span></a>
                <ul >
                    <li><a href="{{ route('about_manage') }}">MANAGEMENT TEAM</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_insurance') }}">INSURANCE</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_bond') }}">BOND</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('about_safety') }}">HEALTH &amp; SAFETY</a></li>
                </ul></li>
            <li id="liServices" class="off"><a href="#"><span>Services</span></a>
                <ul>
                    <li><a href="{{ route('services_demo') }}">DEMOLITION/DISMANTLING</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('services_carting') }}">CARTING/RECYCLING</a></li><li><a href="">•</a></li>
                    <li><a href="{{ route('services_cleaning') }}">CONSTRUCTION CLEANING</a></li>
                </ul></li>
            <li id="liExperiences" class="off"><a href="{{ route('experiences') }}"><span>Experiences</span></a>
                <ul>
                    <li><a href=""></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li><a href="{{ route('experiences_project') }}">PROJECT REFERENCE</a></li>
                </ul></li>
            <li id="liLicenses" class="off"><a href="{{ route('licenses') }}"><span>Licenses &amp; Approvals</span></a>
            </li>
            <li id="liContact" class="off"><a href="{{ route('contact') }}"><span>Contact</span></a>
            </li>
            <li id="liLinks" class="off"><a href="{{ route('links') }}"><span>Related Links</span></a>
            </li>
        </ul>
        <img src="{{ asset('images/main_project.jpg') }}" alt="" width="840" height="200" /><br />
        <img src="{{ asset('images/bar_top.gif') }}" width="840" height="17" /> </div><div id="body">
        <div id="img"><img src="{{ asset('images/TSD_header_project_reference.jpg') }}" alt="" width="350" /></div>
        <div id="column1">
            <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                {{-- START --}}

                @foreach($projects as $project)

                    <tr>
                        <th scope="row"><hr /></th>
                    </tr>
                    <tr>
                        <th scope="row"><br />
                            <table width="100%" border="0" cellpadding="2" cellspacing="0" class="p_table">
                                <tr>
                                    <th width="25%" align="left" valign="top" scope="row"><strong>Company Name: </strong></th>
                                    <td width="75%" align="left" valign="top">{{ $project['company_name'] }}</td>
                                    <td width="75%" rowspan="7" align="center" valign="top"><img src="{{ isset($project['image']) ? asset('/images/projects/'. $project['image'])  : 'https://via.placeholder.com/180x150.png?text=Placeholder' }}" alt="" width="180" height="150" /></td>
                                </tr>
                                <tr>
                                    <th align="left" valign="top" scope="row"><strong>Address: </strong></th>
                                    <td align="left" valign="top">{{ $project['address_line_1'] ?? '' }}<br />
                                        {{ $project['address_line_2'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th align="left" valign="top" scope="row"><strong>Project: </strong></th>
                                    <td align="left" valign="top">{{ $project['project_line_1'] ?? '' }} @isset($project['project_line_2']) <br/>{{$project['project_line_2']  }} @endisset</td>
                                </tr>
                                <tr>
                                    <th align="left" valign="top" scope="row"><strong>Year performed: </strong></th>
                                    <td align="left" valign="top">{{ $project['year'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th align="left" valign="top" scope="row"><strong>Price: </strong></th>
                                    <td align="left" valign="top">{{ $project['price'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th align="left" valign="top" scope="row"><strong>Description: </strong></th>
                                    <td align="left" valign="top">{{ $project['description'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th align="left" valign="top" scope="row"><strong>Contact: </strong></th>
                                    <td align="left" valign="top">{{ $project['contact'] ?? '' }}</td>
                                </tr>
                            </table></th>
                    </tr>

                @endforeach

                {{-- END --}}

                {{--<tr>--}}
                    {{--<th scope="row"><hr /></th>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<th scope="row"><br />--}}
                        {{--<table width="100%" border="0" cellpadding="2" cellspacing="0" class="p_table">--}}
                            {{--<tr>--}}
                                {{--<th width="25%" align="left" valign="top" scope="row"><strong>Company Name: </strong></th>--}}
                                {{--<td width="75%" align="left" valign="top">RC Dolner LLC</td>--}}
                                {{--<td width="75%" rowspan="7" align="center" valign="top"><img src="{{ asset('images/projects/8.jpg') }}" alt="" width="180" height="150" /></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th align="left" valign="top" scope="row"><strong>Address: </strong></th>--}}
                                {{--<td align="left" valign="top">15-17 East 17th Street<br />--}}
                                    {{--New York, NY 10003</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th align="left" valign="top" scope="row"><strong>Project: </strong></th>--}}
                                {{--<td align="left" valign="top">Metropolitan Museum of Art<br />--}}
                                    {{--Wing K &amp; 19 Century Galleries<br />--}}
                                    {{--American Wing<br />--}}
                                    {{--Lehman Wing<br />--}}
                                    {{--WingD<br />--}}
                                    {{--WingK</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th align="left" valign="top" scope="row"><strong>Year performed: </strong></th>--}}
                                {{--<td align="left" valign="top">2004/2009 </td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th align="left" valign="top" scope="row"><strong>Price: </strong></th>--}}
                                {{--<td align="left" valign="top">$4,519,000.00</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th align="left" valign="top" scope="row"><strong>Description: </strong></th>--}}
                                {{--<td align="left" valign="top">Interior demolition </td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th align="left" valign="top" scope="row"><strong>Contact: </strong></th>--}}
                                {{--<td align="left" valign="top">Mr. Michael Jones (212) 645 2190</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<th colspan="3" align="left" valign="top" scope="row"><hr /></th>--}}
                            {{--</tr>--}}

                        {{--</table></th>--}}
                {{--</tr>--}}
            </table>
            &nbsp;
        </div><div id="body2"><img src="{{ asset('images/bar_bottom.gif') }}" width="840" height="15" /></div></div><div id="footer" class="footer">Copyright © 2003-2010 TSD, All Rights Reserved. | website created by <a href="http://www.bermangrp.com" target="_blank"><span style="color:#FFF">The Berman Group</span></a></div>
</div>

</body>
</html>
