<?php

namespace App\Http\Livewire;

use App\Mail\ContactMessageSubmitted;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class ContactForm extends Component
{

    public $name;
    public $email;
    public $phone;
    public $subject;
    public $message;
    public $recaptcha;

    protected $rules = [
        'name' => 'required|min:3',
        'email' => 'required|email',
        'phone' => 'nullable',
        'subject' => 'required',
        'message' => 'required',
        'recaptcha' => 'recaptcha'
    ];

    public function submit()
    {

        if(count($this->getErrorBag()->all()) > 0){
            $this->emit('resetReCaptcha');
        }

        $validatedData = $this->validate();

        Mail::send('email',
            array(
                'name' => $this->name,
                'subject' => $this->subject,
                'email' => $this->email,
                'phone' => $this->phone,
                'comment' => $this->message,
            ),
            function($message){
                $message->from('mail@gotsd.com');
                $message->to('mail@gotsd.com', 'TSD')->replyTo($this->email, $this->name)->subject('TSD Contact Form');
            }
        );

        $this->dispatchBrowserEvent('closeModal');

        $this->name = '';
        $this->email = '';
        $this->subject = '';
        $this->message = '';
        $this->recaptcha = '';
        $this->phone = '';

        $this->emit('resetReCaptcha');


    }

    public function render()
    {
        return view('livewire.contact-form');
    }
}
